package com.suslinproduction.woodman.Controller;

import com.suslinproduction.woodman.DTI.CatalogDTI.CatalogDTI;
import com.suslinproduction.woodman.DTO.CatalogDTO.CatalogDTO;
import com.suslinproduction.woodman.Entity.Catalog;
import com.suslinproduction.woodman.Interface.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/catalog")
public class CatalogController {


    @Autowired
    private CatalogService catalogService;

    @PostMapping()
    public ResponseEntity<CatalogDTO> createCatalog(@Valid @RequestBody CatalogDTI catalog) {
        return catalogService.createCatalog(catalog);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CatalogDTO> updateCatalog(@PathVariable(value = "id")int id,@Valid @RequestBody Catalog catalog) {
        return catalogService.updateCatalog(id,catalog);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CatalogDTO> deleteCatalog(@PathVariable(value = "id") int id) {
        return catalogService.deleteCatalog(id);
    }

    @GetMapping()
    public ResponseEntity<List<CatalogDTO>> getCatalogList() {
        return catalogService.getCatalogList();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CatalogDTO> getCatalogById(@PathVariable(value = "id") int id){
        return catalogService.getCatalogById(id);
    }

    @PutMapping("/AddProductToCatalog/{id}")
    public ResponseEntity addProductToCatalog(@PathVariable(value = "id")int id,int[] productId){
        return catalogService.addProductsToTheCatalog(productId,id);
    }
}

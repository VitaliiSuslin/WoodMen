package com.suslinproduction.woodman.Controller;


import com.suslinproduction.woodman.DTI.ConsignmentDTI.ConsignmentDTI;
import com.suslinproduction.woodman.DTO.ConsignmentDTO.ConsignmentDTO;
import com.suslinproduction.woodman.Entity.Consignment;
import com.suslinproduction.woodman.Interface.ConsignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/consignment")
public class ConsignmentController{

    @Autowired
    private ConsignmentService consignmentService;

    @PostMapping()
    public ResponseEntity<ConsignmentDTO> createConsignment(@Valid @RequestBody ConsignmentDTI consignment) {
        return consignmentService.createConsignment(consignment);
    }

   @GetMapping("/{id}")
    public ResponseEntity<ConsignmentDTO> getConsignmentById(@PathVariable(value = "id") int id) {
        return consignmentService.getConsignmentById(id);
    }

    @GetMapping()
    public ResponseEntity<List<ConsignmentDTO>> getConsignment() {
        return consignmentService.getConsignment();
    }

}

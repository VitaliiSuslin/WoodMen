package com.suslinproduction.woodman.Controller;

import com.suslinproduction.woodman.DTI.NewsDTI.NewsDTI;
import com.suslinproduction.woodman.Entity.News;
import com.suslinproduction.woodman.Interface.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @PostMapping()
    public ResponseEntity<News> createNews(@Valid @RequestBody NewsDTI news) {
        return newsService.createNews(news);
    }

   @PutMapping()
    public ResponseEntity<News> updateNews(@Valid @RequestBody News news) {
        return newsService.updateNews(news);
    }

   @DeleteMapping("/{id}")
    public ResponseEntity<News> deleteNews(@PathVariable(value = "id")int id) {
        return newsService.deleteNews(id);
    }


    @GetMapping("/{id}")
    public ResponseEntity<News> getNewsById(@PathVariable(value = "id")int id) {
        return newsService.getNewsById(id);
    }


    @GetMapping()
    public ResponseEntity<List<News>> getNewsList() {
        return newsService.getNewsList();
    }
}

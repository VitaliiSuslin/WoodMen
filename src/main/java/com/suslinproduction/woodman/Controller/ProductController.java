package com.suslinproduction.woodman.Controller;

import com.suslinproduction.woodman.DTI.ProductDTI.ProductDTI;
import com.suslinproduction.woodman.DTO.ProductDTO.ProductDTO;
import com.suslinproduction.woodman.Entity.Product;
import com.suslinproduction.woodman.Interface.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping()
    public ResponseEntity<ProductDTO> createProduct(@Valid @RequestBody ProductDTI product) {
        return productService.createProduct(product);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable(value = "id")int id,@Valid @RequestBody ProductDTI product) {
        return productService.updateProduct(id,product);
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> getProductList() {
        return productService.getProductList();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ProductDTO> deleteProduct(@PathVariable(value = "id") int id) {
        return productService.deleteProduct(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable(value = "id")int id) {
        return productService.getProductById(id);
    }
}

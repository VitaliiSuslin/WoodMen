package com.suslinproduction.woodman.Controller;

import com.suslinproduction.woodman.DTO.UserDTO.UserDTO;
import com.suslinproduction.woodman.Entity.User;
import com.suslinproduction.woodman.Interface.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController{

    @Autowired
    private UserService userService;

    @PostMapping()
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody User user) {
        return userService.createUser(user);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> findUserById(@PathVariable(value = "id") Long id) {
        return userService.findUserById(id);
    }

    @GetMapping("/findUserByEmail/{email}")
    public ResponseEntity<UserDTO> findUserByEmail(@PathVariable(value = "email")String email) {
        return userService.findUserByEmail(email);
    }


    @GetMapping("/findUserByNumberPhone/{numberPhone}")
    public ResponseEntity<UserDTO> findUserByNumberPhone(@PathVariable(value = "numberPhone") String numberPhone) {
        return userService.findUserByNumberPhone(numberPhone);
    }
}

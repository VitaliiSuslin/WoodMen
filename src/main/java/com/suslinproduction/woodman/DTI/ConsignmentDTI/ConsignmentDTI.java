package com.suslinproduction.woodman.DTI.ConsignmentDTI;

import com.suslinproduction.woodman.Entity.Product;

import java.util.List;

public class ConsignmentDTI {
    private String userFullName;
    private String email;
    private String numberPhone;
    private double price;
    private List<Integer> products;

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Integer> getProducts() {
        return products;
    }

    public void setProducts(List<Integer> products) {
        this.products = products;
    }
}

package com.suslinproduction.woodman.DTI.NewsDTI;

import java.util.ArrayList;

public class NewsDTI {
    private String name;
    private String description;
    private ArrayList<String> photoUrls;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getPhotoUrls() {
        return photoUrls;
    }

    public void setPhotoUrls(ArrayList<String> photoUrls) {
        this.photoUrls = photoUrls;
    }
}

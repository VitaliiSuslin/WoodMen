package com.suslinproduction.woodman.DTO.CatalogDTO;

import com.suslinproduction.woodman.DTO.ProductDTO.ProductDTO;

import java.util.List;

public class CatalogDTO {
    private int id;
    private String Name;

    private List<ProductDTO> productList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public List<ProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDTO> productList) {
        this.productList = productList;
    }
}

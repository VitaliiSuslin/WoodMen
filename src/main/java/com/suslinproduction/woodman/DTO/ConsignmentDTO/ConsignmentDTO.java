package com.suslinproduction.woodman.DTO.ConsignmentDTO;

import com.suslinproduction.woodman.DTO.ProductDTO.ProductDTO;

import java.util.List;

public class ConsignmentDTO {
    private int id;
    private Long userId;
    private List<ProductDTO> productList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<ProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDTO> productList) {
        this.productList = productList;
    }
}

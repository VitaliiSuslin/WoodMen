package com.suslinproduction.woodman.DTO.UserDTO;

import java.util.List;

public class UserDTO {
    private Long id;
    private String fullName;
    private String email;
    private String numberPhone;
    private List<Integer> consignmentsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public List<Integer> getConsignmentsId() {
        return consignmentsId;
    }

    public void setConsignmentsId(List<Integer> consignmentsId) {
        this.consignmentsId = consignmentsId;
    }
}

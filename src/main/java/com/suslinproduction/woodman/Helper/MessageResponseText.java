package com.suslinproduction.woodman.Helper;

public class MessageResponseText {

    public static String GREETING_UKRAINIAN_TEXT = "Шановний ";
    public static String GREETING_ENGLIXH_TEXT = "Dear ";
    public static String AUTOMATIC_MESSAGE_UKRAINIAN_TEXT = "\n\n\n" +
        "П.С.\n" +
        "Це є автоматичне повідомлення!!!Будь-ласка, не відповідайте на нього.";
    public static String AUTOMATIC_MESSAGE_ENGLISH_TEXT = "\n\n\n" +
        "P.S.\n" +
        "This is automatic message!!! Please, don`t answer that!!!";
    private String consignmentId;
    private String price;
    public final String CONSIGNMENT_EMAIL_UKRAINIAN_TEXT = "Ваше замовлення ID:" + consignmentId + " із загальною сумою "
        + price + "₴"
        + "\nОчікуйте дзвінка протягом тижня."
        + "\nДякуємо, що обрали нас!!!"

        + AUTOMATIC_MESSAGE_UKRAINIAN_TEXT;
    public final String CONSIGNMENT_EMAIL_ENGLISH_TEXT = "Your consignment ID:" + consignmentId + " with price "
        + price + "₴"
        + "\nWe call you during a week."
        + "\nThank you for your choose!!!"

        + AUTOMATIC_MESSAGE_ENGLISH_TEXT;

    public MessageResponseText() {

    }

    public void setConsignmentProperties(int consignmentId, double price) {
        this.consignmentId = String.valueOf(consignmentId);
        this.price = String.valueOf(price);
    }
}

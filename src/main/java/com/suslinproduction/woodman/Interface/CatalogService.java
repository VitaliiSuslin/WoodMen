package com.suslinproduction.woodman.Interface;

import com.suslinproduction.woodman.DTI.CatalogDTI.CatalogDTI;
import com.suslinproduction.woodman.DTO.CatalogDTO.CatalogDTO;
import com.suslinproduction.woodman.Entity.Catalog;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CatalogService {
    ResponseEntity<CatalogDTO> createCatalog(CatalogDTI catalog);

    ResponseEntity<CatalogDTO> updateCatalog(int id, Catalog catalog);

    ResponseEntity<CatalogDTO> getCatalogById(int id);

    ResponseEntity<CatalogDTO> deleteCatalog(int catalogId);

    ResponseEntity<List<CatalogDTO>> getCatalogList();


    ResponseEntity addProductsToTheCatalog(int productId[], int catalogId);

    ResponseEntity deleteProductInCatalog(int productId, int catalogId);
}

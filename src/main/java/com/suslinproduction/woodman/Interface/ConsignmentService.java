package com.suslinproduction.woodman.Interface;

import com.suslinproduction.woodman.DTI.ConsignmentDTI.ConsignmentDTI;
import com.suslinproduction.woodman.DTO.ConsignmentDTO.ConsignmentDTO;
import com.suslinproduction.woodman.Entity.Consignment;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ConsignmentService {
    ResponseEntity<ConsignmentDTO> createConsignment(ConsignmentDTI consignment);

    ResponseEntity<ConsignmentDTO> getConsignmentById(int id);

    ResponseEntity<List<ConsignmentDTO>> getConsignment();
}

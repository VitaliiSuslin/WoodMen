package com.suslinproduction.woodman.Interface;

import com.suslinproduction.woodman.Entity.Message;

public interface MessageService {
    boolean sendSimpleEmailMessage(Message message);
    void sendSimpleEmailNewsMessage(Message message);
}

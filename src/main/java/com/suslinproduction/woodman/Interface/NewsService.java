package com.suslinproduction.woodman.Interface;

import com.suslinproduction.woodman.DTI.NewsDTI.NewsDTI;
import com.suslinproduction.woodman.Entity.News;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface NewsService {
    ResponseEntity<News> createNews(NewsDTI news);

    ResponseEntity<News> updateNews(News news);

    ResponseEntity<News> deleteNews(int newsId);

    ResponseEntity<News> getNewsById(int id);

    ResponseEntity<List<News>> getNewsList();
}

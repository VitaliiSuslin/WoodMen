package com.suslinproduction.woodman.Interface;

import com.suslinproduction.woodman.DTI.ProductDTI.ProductDTI;
import com.suslinproduction.woodman.DTO.ProductDTO.ProductDTO;
import com.suslinproduction.woodman.Entity.Product;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {
    ResponseEntity<ProductDTO> createProduct(ProductDTI product);

    ResponseEntity<ProductDTO> updateProduct(int productId,ProductDTI product);

    ResponseEntity<List<ProductDTO>> getProductList();

    ResponseEntity<ProductDTO> deleteProduct(int productId);

    ResponseEntity<ProductDTO> getProductById(int productId);
}

package com.suslinproduction.woodman.Interface;

import com.suslinproduction.woodman.DTO.UserDTO.UserDTO;
import com.suslinproduction.woodman.Entity.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<UserDTO> createUser(User user);

    ResponseEntity<UserDTO> findUserById(Long userID);

    ResponseEntity<UserDTO> findUserByEmail(String email);

    ResponseEntity<UserDTO> findUserByNumberPhone(String numberPhone);
}

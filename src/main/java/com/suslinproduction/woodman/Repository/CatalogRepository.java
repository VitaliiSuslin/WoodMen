package com.suslinproduction.woodman.Repository;

import com.suslinproduction.woodman.Entity.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogRepository extends JpaRepository<Catalog,Integer> {

}

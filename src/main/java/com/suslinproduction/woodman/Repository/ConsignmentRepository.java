package com.suslinproduction.woodman.Repository;

import com.suslinproduction.woodman.Entity.Consignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsignmentRepository extends JpaRepository<Consignment,Integer> {
}

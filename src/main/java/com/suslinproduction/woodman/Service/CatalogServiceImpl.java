package com.suslinproduction.woodman.Service;

import com.suslinproduction.woodman.DTI.CatalogDTI.CatalogDTI;
import com.suslinproduction.woodman.DTO.CatalogDTO.CatalogDTO;
import com.suslinproduction.woodman.DTO.ProductDTO.ProductDTO;
import com.suslinproduction.woodman.Entity.Catalog;
import com.suslinproduction.woodman.Entity.Product;
import com.suslinproduction.woodman.Interface.CatalogService;
import com.suslinproduction.woodman.Repository.CatalogRepository;
import com.suslinproduction.woodman.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class CatalogServiceImpl implements CatalogService {

    @Autowired
    private CatalogRepository catalogRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public ResponseEntity<CatalogDTO> createCatalog(CatalogDTI catalog) {
        Catalog tempCatalog = new Catalog();
        tempCatalog.setName(catalog.getName());
        Catalog newCatalog = catalogRepository.save(tempCatalog);
        if (newCatalog == null) {
            return ResponseEntity.badRequest().build();
        }
        CatalogDTO catalogDTO=new CatalogDTO();
        catalogDTO.setId(newCatalog.getId());
        catalogDTO.setName(newCatalog.getName());
        return ResponseEntity.ok(catalogDTO);
    }

    @Override
    public ResponseEntity<CatalogDTO> updateCatalog(int id, Catalog catalog) {
        Catalog catalog1 = catalogRepository.findOne(id);
        if (catalog1 == null) {
            return ResponseEntity.notFound().build();
        }
        catalog1.setName(catalog.getName());
        catalog1.setProducts(catalog.getProducts());
        Catalog updateCatalog = catalogRepository.save(catalog1);
        if (updateCatalog == null) {
            return ResponseEntity.badRequest().build();
        }
        CatalogDTO catalogDTO=new CatalogDTO();
        catalogDTO.setId(updateCatalog.getId());
        catalogDTO.setName(updateCatalog.getName());
        return ResponseEntity.ok(catalogDTO);
    }

    @Override
    public ResponseEntity<CatalogDTO> getCatalogById(int id) {
        Catalog catalog = catalogRepository.findOne(id);
        if (catalog == null) {
            return ResponseEntity.notFound().build();
        }
        CatalogDTO catalogDTO=new CatalogDTO();
        catalogDTO.setId(catalog.getId());
        catalogDTO.setName(catalog.getName());
        List<Product> productList=catalog.getProducts();
        LinkedList<ProductDTO> productDTOList=new LinkedList<>();
        for (Product item:productList
             ) {
            ProductDTO productDTO=new ProductDTO();
            productDTO.setId(item.getId());
            productDTO.setName(item.getName());
            productDTO.setDescription(item.getDescription());
            productDTO.setPrice(item.getPrice());
            productDTO.setPhotoUrls(item.getPhotoUrls());
            productDTOList.add(productDTO);
        }
        catalogDTO.setProductList(productDTOList);
        return ResponseEntity.ok(catalogDTO);
    }

    @Override
    public ResponseEntity<CatalogDTO> deleteCatalog(int catalogId) {
        Catalog catalog = catalogRepository.findOne(catalogId);
        if (catalog == null) {
            return ResponseEntity.notFound().build();
        }
        catalogRepository.delete(catalog);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<CatalogDTO>> getCatalogList() {
        List<Catalog> catalogList = catalogRepository.findAll();
        LinkedList<CatalogDTO> catalogDTOList=new LinkedList<>();
        for (Catalog item:catalogList
             ) {
            CatalogDTO catalogDTO=new CatalogDTO();
            catalogDTO.setId(item.getId());
            catalogDTO.setName(item.getName());
            catalogDTOList.add(catalogDTO);
        }
        return ResponseEntity.ok(catalogDTOList);
    }

    /*Need to think
     *
     * todo:Complete method to adding products to the catalog
     * */
    @Override
    public ResponseEntity addProductsToTheCatalog(int[] productId, int catalogId) {
        Catalog catalog = catalogRepository.findOne(catalogId);
        if (catalog == null) {
            return ResponseEntity.notFound().build();
        }

        List<Product> products = new ArrayList<>();
        for (int i : productId
            ) {
            Product product = productRepository.findOne(i);
            if(product==null){return ResponseEntity.notFound().build();}
            products.add(product);
        }

        catalog.setProducts(products);
        catalogRepository.save(catalog);
        return ResponseEntity.ok().build();
    }

    /*
     * Need to think
     * todo:Complete method to delete product in the catalog
     * */
    @Override
    public ResponseEntity deleteProductInCatalog(int productId, int catalogId) {
        Product product = productRepository.findOne(productId);
        if (product == null) {
            return ResponseEntity.notFound().build();
        }
        Catalog catalog = catalogRepository.findOne(catalogId);
        if (catalog == null) {
            return ResponseEntity.notFound().build();
        }


        return null;
    }
}

package com.suslinproduction.woodman.Service;

import com.suslinproduction.woodman.DTI.ConsignmentDTI.ConsignmentDTI;
import com.suslinproduction.woodman.DTO.ConsignmentDTO.ConsignmentDTO;
import com.suslinproduction.woodman.DTO.ProductDTO.ProductDTO;
import com.suslinproduction.woodman.Entity.Consignment;
import com.suslinproduction.woodman.Entity.Message;
import com.suslinproduction.woodman.Entity.Product;
import com.suslinproduction.woodman.Entity.User;
import com.suslinproduction.woodman.Helper.MessageResponseText;
import com.suslinproduction.woodman.Interface.ConsignmentService;
import com.suslinproduction.woodman.Interface.MessageService;
import com.suslinproduction.woodman.Repository.ConsignmentRepository;
import com.suslinproduction.woodman.Repository.ProductRepository;
import com.suslinproduction.woodman.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class ConsignmentServiceImpl implements ConsignmentService {


    private MessageResponseText messageResponseText = new MessageResponseText();

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConsignmentRepository consignmentRepository;
    @Autowired
    private MessageService messageService;


    @Autowired
    private ProductRepository productRepository;

    @Override
    public ResponseEntity<ConsignmentDTO> createConsignment(ConsignmentDTI consignment) {

        User user = userRepository.findUserByNumberPhone(consignment.getNumberPhone());
        if (user == null) {
            User tempUser = new User();
            tempUser.setFullName(consignment.getUserFullName());
            tempUser.setEmail(consignment.getEmail());
            tempUser.setNumberPhone(consignment.getNumberPhone());
            user = userRepository.save(tempUser);
            if (user == null) {
                return ResponseEntity.badRequest().build();
            }
        }
        List<Product> productList = new ArrayList<>();
        for (int i : consignment.getProducts()
            ) {
            productList.add(productRepository.findOne(i));
        }

        Consignment tempConsignment = new Consignment();
        tempConsignment.setUser(user);
        tempConsignment.setProducts(productList);
        tempConsignment.setPrice(consignment.getPrice());

        Consignment newConsignment = consignmentRepository.save(tempConsignment);
        if (newConsignment == null) {
            return ResponseEntity.badRequest().build();
        }

        Message message = new Message();
        message.setUserFullName(user.getFullName());
        message.setEmail(user.getEmail());
        message.setSubject("Wood Man Shop!");
        messageResponseText.setConsignmentProperties(newConsignment.getId(), newConsignment.getPrice());
        message.setDescription(messageResponseText.CONSIGNMENT_EMAIL_UKRAINIAN_TEXT
        );
        boolean send = messageService.sendSimpleEmailMessage(message);
        if (!send) {
            String s = "Message send: ";
            String s2 = "-- false to send message!!!";
            Log.getLog(s, s2, 1);
            System.out.println(s + s2);
        }

        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<ConsignmentDTO> getConsignmentById(int id) {
        Consignment consignment = consignmentRepository.findOne(id);
        if (consignment == null) {
            return ResponseEntity.notFound().build();
        }
        ConsignmentDTO consignmentDTO = new ConsignmentDTO();
        consignmentDTO.setId(consignment.getId());
        consignmentDTO.setUserId(consignment.getUser().getId());

        List<Product> productList = consignment.getProducts();
        LinkedList<ProductDTO> productDTOList = ProductListHandler(productList);
        consignmentDTO.setProductList(productDTOList);
        return ResponseEntity.ok(consignmentDTO);
    }

    @Override
    public ResponseEntity<List<ConsignmentDTO>> getConsignment() {
        List<Consignment> consignments = consignmentRepository.findAll();
        List<ConsignmentDTO> consignmentDTOList = new LinkedList<>();
        for (Consignment item : consignments
            ) {
            ConsignmentDTO consignmentDTO = new ConsignmentDTO();
            consignmentDTO.setId(item.getId());
            consignmentDTO.setUserId(item.getUser().getId());
            List<Product> productList = item.getProducts();
            LinkedList<ProductDTO> productDTOList = ProductListHandler(productList);
            consignmentDTO.setProductList(productDTOList);
            consignmentDTOList.add(consignmentDTO);
        }
        return ResponseEntity.ok(consignmentDTOList);
    }

    private LinkedList<ProductDTO> ProductListHandler(List<Product> productList) {
        LinkedList<ProductDTO> productDTOS = new LinkedList<>();
        for (Product s : productList
            ) {
            ProductDTO productDTO = new ProductDTO();
            productDTO.setId(s.getId());
            productDTO.setName(s.getName());
            productDTO.setPrice(s.getPrice());
            productDTOS.add(productDTO);
        }
        return productDTOS;
    }
}

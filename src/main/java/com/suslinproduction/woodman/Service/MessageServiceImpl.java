package com.suslinproduction.woodman.Service;

import com.suslinproduction.woodman.Entity.Message;
import com.suslinproduction.woodman.Entity.User;
import com.suslinproduction.woodman.Helper.MessageResponseText;
import com.suslinproduction.woodman.Interface.MessageService;
import com.suslinproduction.woodman.Repository.MessageRepository;
import com.suslinproduction.woodman.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    private static String WOODMAN_EMAIL = "wood.man.studio.13@gmail.com";
    private MessageResponseText messageResponseText=new MessageResponseText();

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public boolean sendSimpleEmailMessage(Message message) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(message.getEmail());
            mailMessage.setFrom(WOODMAN_EMAIL);
            mailMessage.setSubject(message.getSubject());
            mailMessage.setText(MessageResponseText.GREETING_UKRAINIAN_TEXT + message.getUserFullName() + "\n\n" + message.getDescription());
            javaMailSender.send(mailMessage);
            Message tempMessage = messageRepository.save(message);
            if (tempMessage == null) {
                return false;
            }
            return true;
        } catch (MailException e) {
            e.printStackTrace();

            return false;
        }
    }

    @Override
    public void sendSimpleEmailNewsMessage(Message message) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom(WOODMAN_EMAIL);
            mailMessage.setSubject(message.getSubject());
            mailMessage.setText(message.getDescription()+MessageResponseText.AUTOMATIC_MESSAGE_UKRAINIAN_TEXT);
            List<User> userList = userRepository.findAll();
            for (User user : userList
                ) {
                mailMessage.setTo(user.getEmail());
                javaMailSender.send(mailMessage);
            }
        } catch (MailException e) {
            e.printStackTrace();
        }


    }
}

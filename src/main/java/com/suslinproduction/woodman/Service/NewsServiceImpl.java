package com.suslinproduction.woodman.Service;

import com.suslinproduction.woodman.DTI.NewsDTI.NewsDTI;
import com.suslinproduction.woodman.Entity.Message;
import com.suslinproduction.woodman.Entity.News;
import com.suslinproduction.woodman.Interface.NewsService;
import com.suslinproduction.woodman.Repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private MessageServiceImpl messageService;


    @Override
    public ResponseEntity<News> createNews(NewsDTI news) {
        News news1=new News();
        news1.setName(news.getName());
        news1.setDescription(news.getDescription());
        news1.setPhotoUrls(news.getPhotoUrls());
        News newNews = newsRepository.save(news1);
        if (newNews == null) {
            return ResponseEntity.badRequest().build();
        }

        Message message=new Message();
        message.setSubject("Wood Man News!");
        message.setDescription(newNews.getDescription());

        messageService.sendSimpleEmailNewsMessage(message);

        return ResponseEntity.ok(newNews);
    }

    @Override
    public ResponseEntity<News> updateNews(News news) {
        News news1 = newsRepository.findOne(news.getId());
        if (news1 == null) {
            return ResponseEntity.notFound().build();
        }
        news1.setDescription(news.getDescription());
        news1.setName(news.getName());
        news1.setPhotoUrls(news.getPhotoUrls());
        News updateNews = newsRepository.save(news1);
        return ResponseEntity.ok(updateNews);
    }

    @Override
    public ResponseEntity<News> deleteNews(int newsId) {
        News news = newsRepository.findOne(newsId);
        if (news == null) {
            return ResponseEntity.notFound().build();
        }
        newsRepository.delete(news);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<News> getNewsById(int id) {
        News news = newsRepository.findOne(id);
        if (news == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(news);
    }

    @Override
    public ResponseEntity<List<News>> getNewsList() {
        List<News> newsList = newsRepository.findAll();
        return ResponseEntity.ok(newsList);
    }
}

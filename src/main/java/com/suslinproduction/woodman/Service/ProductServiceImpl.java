package com.suslinproduction.woodman.Service;

import com.suslinproduction.woodman.DTI.ProductDTI.ProductDTI;
import com.suslinproduction.woodman.DTO.ProductDTO.ProductDTO;
import com.suslinproduction.woodman.Entity.Product;
import com.suslinproduction.woodman.Interface.ProductService;
import com.suslinproduction.woodman.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;


    @Override
    public ResponseEntity<ProductDTO> createProduct(ProductDTI product) {

        Product tempProduct = new Product();
        tempProduct.setName(product.getName());
        tempProduct.setDescription(product.getDescription());
        tempProduct.setPrice(product.getPrice());
        tempProduct.setPhotoUrls(product.getPhotoUrls());
        Product newProduct = productRepository.save(tempProduct);
        if (newProduct == null) {
            return ResponseEntity.badRequest().build();
        }
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(newProduct.getId());
        productDTO.setDescription(newProduct.getDescription());
        productDTO.setName(newProduct.getName());
        productDTO.setPhotoUrls(newProduct.getPhotoUrls());
        productDTO.setPrice(newProduct.getPrice());
        return ResponseEntity.ok(productDTO);
    }

    @Override
    public ResponseEntity<ProductDTO> updateProduct(int productId, ProductDTI product) {
        Product product1 = productRepository.findOne(productId);
        if (product1 == null) {
            return ResponseEntity.notFound().build();
        }
        product1.setName(product.getName());
        product1.setDescription(product.getDescription());
        product1.setPrice(product.getPrice());
        product1.setPhotoUrls(product.getPhotoUrls());
        Product updateProduct = productRepository.save(product1);
        if (updateProduct == null) {
            return ResponseEntity.badRequest().build();
        }
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(updateProduct.getId());
        productDTO.setName(updateProduct.getName());
        productDTO.setDescription(updateProduct.getDescription());
        productDTO.setPhotoUrls(updateProduct.getPhotoUrls());
        productDTO.setPrice(updateProduct.getPrice());
        return ResponseEntity.ok(productDTO);
    }

    @Override
    public ResponseEntity<List<ProductDTO>> getProductList() {
        List<Product> productList = productRepository.findAll();
        LinkedList<ProductDTO> productDTOS = new LinkedList<>();
        for (Product item : productList
            ) {
            ProductDTO productDTO = new ProductDTO();
            productDTO.setId(item.getId());
            productDTO.setName(item.getName());
            productDTO.setPrice(item.getPrice());
            productDTO.setPhotoUrls(item.getPhotoUrls());
//            productDTO.setCatalogId(item.getCatalog().getId());
//            productDTO.setCatalogName(item.getCatalog().getName());
        }
        return ResponseEntity.ok(productDTOS);
    }

    @Override
    public ResponseEntity<ProductDTO> deleteProduct(int productId) {
        Product product = productRepository.findOne(productId);
        if (product == null) {
            return ResponseEntity.notFound().build();
        }
        productRepository.delete(product);
        ProductDTO productDTO=new ProductDTO();
        productDTO.setName(product.getName());
        return ResponseEntity.ok(productDTO);
    }

    @Override
    public ResponseEntity<ProductDTO> getProductById(int productId) {
        Product product = productRepository.findOne(productId);
        if (product == null) {
            return ResponseEntity.notFound().build();
        }
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setDescription(product.getDescription());
        productDTO.setPrice(product.getPrice());
        productDTO.setPhotoUrls(product.getPhotoUrls());
//        productDTO.setCatalogId(product.getCatalog().getId());
//        productDTO.setCatalogName(product.getCatalog().getName());
        return ResponseEntity.ok(productDTO);
    }
}

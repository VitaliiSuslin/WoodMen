package com.suslinproduction.woodman.Service;

import com.suslinproduction.woodman.DTO.UserDTO.UserDTO;
import com.suslinproduction.woodman.Entity.Consignment;
import com.suslinproduction.woodman.Entity.User;
import com.suslinproduction.woodman.Interface.UserService;
import com.suslinproduction.woodman.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseEntity<UserDTO> createUser(User user) {
        User newUser = userRepository.save(user);
        if (newUser == null) {
            return ResponseEntity.badRequest().build();
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setId(newUser.getId());
        userDTO.setFullName(newUser.getFullName());
        userDTO.setEmail(newUser.getEmail());
        userDTO.setNumberPhone(newUser.getNumberPhone());
        return ResponseEntity.ok(userDTO);
    }

    @Override
    public ResponseEntity<UserDTO> findUserById(Long userID) {
        User user = userRepository.findOne(userID);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        List<Integer> consignmentsId = new ArrayList<>();
        List<Consignment> consignmentList = user.getConsignmentList();
        for (Consignment item : consignmentList
            ) {
            consignmentsId.add(item.getId());
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFullName(user.getFullName());
        userDTO.setNumberPhone(user.getNumberPhone());
        userDTO.setEmail(user.getEmail());
        userDTO.setConsignmentsId(consignmentsId);
        return ResponseEntity.ok(userDTO);
    }

    @Override
    public ResponseEntity<UserDTO> findUserByEmail(String email) {
        User user = userRepository.findUserByEmail(email);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        List<Integer> consignmentsId = new ArrayList<>();
        List<Consignment> consignmentList = user.getConsignmentList();
        for (Consignment item : consignmentList
            ) {
            consignmentsId.add(item.getId());
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFullName(user.getFullName());
        userDTO.setEmail(user.getEmail());
        userDTO.setNumberPhone(user.getNumberPhone());
        userDTO.setConsignmentsId(consignmentsId);
        return ResponseEntity.ok(userDTO);
    }

    @Override
    public ResponseEntity<UserDTO> findUserByNumberPhone(String numberPhone) {
        User user = userRepository.findUserByNumberPhone(numberPhone);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        List<Integer> consignmentsId = new ArrayList<>();
        List<Consignment> consignmentList = user.getConsignmentList();
        for (Consignment item : consignmentList
            ) {
            consignmentsId.add(item.getId());
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFullName(user.getFullName());
        userDTO.setEmail(user.getEmail());
        userDTO.setNumberPhone(user.getNumberPhone());
        userDTO.setConsignmentsId(consignmentsId);
        return ResponseEntity.ok(userDTO);
    }
}

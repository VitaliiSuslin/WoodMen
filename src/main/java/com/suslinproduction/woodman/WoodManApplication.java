package com.suslinproduction.woodman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.TimeZone;

@SpringBootApplication
@EnableJpaAuditing
public class WoodManApplication {

//    @PostConstruct
//    public void init(){
//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//        System.out.println("Spring boot application running in UTC timezone: "+new Date());
//    }


    public static void main(String[] args) {
        SpringApplication.run(WoodManApplication.class, args);
    }
}
